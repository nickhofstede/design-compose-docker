# Using Documentflow #

This guide will show how to install a documentflow project in the docker image that will be started when the server starts.
In this sample the documentflow will provide a rest endpoint using a web input step that allows to generate output for a given template.
This is one of the documentflow samples included in the Design & Compose SHE.

To create the zip file, just export the Design & Compose project from the Designer application using the *export* action and the *Archive* functionality of that action.
You can copy as many documentflow projects as you want, just make sure that they have unique names. Read the section on *Project Descriptor* in the Design & Compose SHE manual for more information.

## Building and running the server ##

```
docker build -t design-compose-documentflow .
```

```
docker run --name design-compose-server-documentflow -p13542:13542 design-compose-documentflow
```

After the run command the server should be available on your dockerhost ip under port 13542.

```
docker stop design-compose-server-documentflow
```

Open your browser and navigate to http://localhost:13542/web/makeoutput?outputformat=pdf&template=barcodes. This will trigger the initial web step and it will generate an output file.
Replace localhost with the ip of the docker host if you run on an external docker server.