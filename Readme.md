# Design Compose Docker Repo #

This repo contains samples files on how to set up docker files for design-compose self hosted edition.
Start with the base image (base folder), after that you can expand this image with extensions, configuration etc.

## Changelog ##

### 21/03 First version ##

Initial version of base image, configuration sample, extension sample and documentflow sample.