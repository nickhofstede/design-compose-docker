# Base image #

Docker is a layered system. These layers allow you to create reusable parts. This section describes how you can create such a layer for the Design & Compse server.
The resulting docker image will be able to start the Design & Compse server. It will also allow to specify some low level configuration that typically changes based on the context where the image is used (local machine, development, production, ...). This configuration corresponds with what is in the Design & Compse server *ini* file in a traditional setup.

# Requirements #

This guide assumes basic knowledge of linux.

You will need to following:

1. **Docker** installation
2. A base **image with Java 11** (preferably AWS Corretto 11 JDK). The *java* command should be available on the path. If you do not have this base container or you are just experimenting you can use *amazoncorretto:11*.
3. **design-compose-21.04.zip**, the zipped distribution of the Design & Compose SHE server.
4. **license.lic** a valid license.
5. A text editor, preferably with docker syntax support

# Dockerfile #

This section describes the various parts of the Dockerfile.
Copy the **license.lic** and **design-compose-21.04.zip** into the base folder next to the dockerfile.

You can build the image using docker build.
```
cd base
docker build .
```

You can check out the result of each build by running the bash shell in the docker image in interactive mode. 
```
docker run -it #hash# bash
```
Where #hash# is the last hash printed by docker build command. Just use the *exit* command to exit the running docker image bash shell and return to the shell of you docker host.

The following sections explain the Dockerfile

## Staged Unzip ##

```
FROM amazonlinux:2 as builder
WORKDIR /tmp
RUN yum install -y unzip
COPY design-compose-21.04.zip design-compose-21.04.zip
RUN unzip design-compose-21.04.zip && \
    rm design-compose-21.04.zip
```

The first step would to unzip the Design & Compse zip file. Since you want to build clean docker layers it is best to use the multi stage technique. By using this technique you can keep the resulting image free of unzip. It will also make that the resulting image is smaller in size. Unzipping will create lots of temporary files, docker might store all these file in the image if you are not careful, this will blow up the image with 100 of megabytes. Smaller docker images matter because when running them they will start faster and they require less bandwidth to fetch/push making them more cost efficient especially when running them in mass.

## Actual base image ##

### Base image ###

```
FROM amazoncorretto:11
```

Specify the base image with Java 11. You can use your own base image.

### Set up Design & Compose Installation folder ###

```
WORKDIR /opt/design-compose
COPY --from=builder /tmp .
```

The next set of commands will
1. Set up the workfolder to the path where we *install* the application
2. Copy the unzipped files from the previous stage to the installation folder

### Add a scriptura User and set up Design & Compose home folder ###

```
RUN yum install -y shadow-utils  && \
    yum clean all && \
    groupadd --gid 1000 dcshe && \
    useradd --uid 1000 --gid dcshe --shell /bin/bash --base-dir /var/lib/app --create-home dcshe && \
    mkdir -p /var/lib/app/dcshe/design-compose
COPY --chown=1000:1000 license.lic /var/lib/app/dcshe/design-compose
```

The installation of shadow-utils is required because it will provide the *groupadd/useradd* commands later. This might not be required in your base image our you might have another command to create groups and users.

The *groupadd* and *useradd* command will create a dcshe user with user and group id 1000. The *User* command will tell docker to use this user when the image is ran.

By creating a user you can do mapping of user when running the docker image. This makes that the image can be run as a user defined on the docker host. When not configured correctly, docker will run images as some default user that can have more privileges than required which can be a security risk.

The other commands will create a **design-compose** folder in the userhome of this new user. This is what the zip install expects.
Next the license file is copied here.

### Expose volumes  ###

```
VOLUME /var/lib/app/dcshe/design-compose
VOLUME /tmp
```

This exposes the */tmp* and Design & Compse home folder as volumes. By doing this you can optionally mount a volume a volume here. For Design & Compse home this is required if you wan to persist the server state between different runs. For */tmp* this can be useful if you use a lot of disk space while processing.

### Env variables ###

````
ENV JVM_XMS="1024m"
ENV JVM_XMX="1024m"
ENV JAVA_OPTS=""
````

These variables allow you to overwrite configuration settings when running the docker image.
You can add more if needed.

### Expose server port ###

```
EXPOSE 13542
```

This exposes the server port. You can map this port to a port on the docker host using *-p* in the docker run command. For example *-p13542:13542* will make the server avaiable on the docker host under port 13542.

### ENTRYPOINT ###

```
./StartServer --launcher.appendVmargs -vmargs \
    -Xms$JVM_XMS \
    -Xmx$JVM_XMX \
    $JAVA_OPTS
```

This is the command that is executed when the docker image is ran. It will start the server. We use the specified environment variables to change overrule some vmarguments specified in the default *StartServer.ini* file.

## Building and running the server ##

```
docker build -t design-compose-base .
```

```
docker run --name design-compose-server -p13542:13542 design-compose-base
```

After the run command the server should be available on your dockerhost ip under port 13542.
To stop or start the server use the following commands.

```
docker stop design-compose-server
```

```
docker start design-compose-server
```

## Next steps ##

All other samples will use this base image and assume its name is *design-compose-base*.
For learning purposes it is best to check out the configuration sample next.
