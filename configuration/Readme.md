# Configuration #

In order to configure the server you will need to provide a **configuration.xml** file.
This file must be placed in the Design & Compose home folder. In the provided base image this folder is **/var/lib/app/dcshe/design-compose**.

## Static configuration ##

The following shows how to copy a local configuration.xml file to the docker image.

```
COPY configuration.xml /var/lib/app/dcshe/design-compose/configuration/configuration.xml
```

## Dynamic configuration ##

In a more complex setup you might want to change the **configurtation.xml** file when running the docker image.
To achieve this you can modify the entrypoint to execute some script before the server is started.

```
COPY configuration.sh configuration.sh
RUN chmod +x configuration.sh
...
ENTRYPOINT ./configuration.sh && \
    ./StartServer --launcher.appendVmargs -vmargs \
    -Xms$JVM_XMS \
    -Xmx$JVM_XMX \
    $JAVA_OPTS
```

Most likely you want to introduce some templating tool for this. For example [confd](https://github.com/kelseyhightower/confd).
This tool allows to provide templates in which variables are replaced from all kinds of configuration sources.
This sample uses this tool to set up a more complex configuration.xml file. We use environment variables to modify behavior when the server is started.

**Note** that this technique can be used with other tools too. *confd* was chosen here because it is very light.

## Building and running the server ##

```
docker build -t design-compose-configuration .
```

```
docker run --name design-compose-server-configuration -p13542:13542 design-compose-configuration
```

After the run command the server should be available on your dockerhost ip under port 13542.

```
docker stop design-compose-server-configuration
```