# Installing extensions #

This dockerfile shows how to install extensions for Design & Compose server. You can use the director application as described [here](http://help.eclipse.org/latest/index.jsp?topic=/org.eclipse.platform.doc.isv/guide/p2_director.html). This docker file starts from the base image. See base folder on how to create that.

Make sure that you have the offical extensions update site available next to the Dockerfile renamed to extensions.zip, you can download these extensions from the product private website.
This sample will install the wbd documentflow steps from this update site. You can add multiple installable units in one run. By specifying version 0.0.0, the latest version will be installed.

```
USER root
COPY extensions.zip /tmp/extensions.zip
RUN ./StartServer \
    -application org.eclipse.equinox.p2.director \
    -repository 'jar:file:/tmp/extensions.zip!/' \
    -installIU com.id.feature.server.wbd.documentflow.feature.group/0.0.0
RUN rm /tmp/extensions.zip
USER 1000:1000
```

**Note** that you must switch back to the *root* user to perform installation changes.


## Building and running the server ##

```
docker build -t design-compose-extensions .
```

```
docker run --name design-compose-server-ext -p13542:13542 design-compose-extensions
```

After the run command the server should be available on your dockerhost ip under port 13542.

```
docker stop design-compose-server-ext
```
